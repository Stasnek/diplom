package diplom.generator;

import diplom.generator.operations.Operation;
import diplom.generator.operations.impl.recursive.Div;
import diplom.generator.operations.impl.simple.Mult;
import diplom.generator.util.MathUtil;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 18.05.2016.
 */
public class GeneratorTest extends TestCase {

    public void testCheckResult() throws Exception {
        List<Operation> operations = new ArrayList<Operation>();
        operations.add(new Mult());
        operations.add(new Div());
        Generator generator = new Generator(new ArrayList<Operation>());
        generator.setRoundValue(10);
        generator.setConstantsWhole(false);
        generator.next();
        double re = generator.getParser().getComplexValue().re();
        double im = generator.getParser().getComplexValue().im();
        int n = 32;
        int y = 2;
        System.out.println("\n" + generator.checkAnswer(MathUtil.round(re, n), MathUtil.round(im, y)));
        System.out.println("actual re" + re);
        System.out.println(" round re" + MathUtil.round(re, n));
        System.out.println(" trunc re" + MathUtil.truncate(re, n));
        System.out.println("actual im " + im);
        System.out.println(" round im " + MathUtil.round(im, y));
        System.out.println(" trunc im " + MathUtil.truncate(im, y));

    }
}