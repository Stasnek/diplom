package diplom.explainer;

import diplom.generator.Generator;
import org.nfunk.jep.JEP;
import org.nfunk.jep.Node;

/**
 * Created by User on 28.05.2016.
 */
public class Explainer {
    public static void test(Generator generator) {
        JEP parser = generator.getParser();
        Node node = parser.getTopNode();
        System.out.println(node.toString());
        nodeDiving(node);

    }

    static void nodeDiving(Node node) {
        for (int i = 0; i < node.jjtGetNumChildren(); i++) {
            System.out.println(node.jjtGetChild(i));
            if (node.jjtGetChild(i).jjtGetNumChildren() > 0) {
                nodeDiving(node.jjtGetChild(i));
            }
        }
    }
}
