package diplom.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

import static diplom.gui.Main.ANSWER_DIALOG_MODAL_FXML;

/**
 * Created by User on 11.05.2016.
 */
public class AnswerController {
    private final static String ANSWER_DIALOG_STAGE_TITLE = "Результат";

    private static Stage answerStage;
    private static Stage rightAnswerStage;
    private static Stage wrongAnswerStage;
    @FXML
    public TextField reTextField;
    @FXML
    public TextField imTextField;


    public static Stage getAnswerStage() {
        return answerStage;
    }

    public static Stage getWrongAnswerStage() {
        return wrongAnswerStage;
    }

    public static void setWrongAnswerStage(Stage wrongAnswerStage) {
        AnswerController.wrongAnswerStage = wrongAnswerStage;
    }

    public static void setAnswerStage(Stage answerStage) {
        AnswerController.answerStage = answerStage;
    }

    public static Stage getRightAnswerStage() {
        return rightAnswerStage;
    }


    public static void setRightAnswerStage(Stage rightAnswerStage) {
        AnswerController.rightAnswerStage = rightAnswerStage;
    }

    private void clearFields() {
        reTextField.clear();
        imTextField.clear();
    }

    private Stage createAnswerStage() {
        Parent answerDialogWindow = null;
        try {
            answerDialogWindow = FXMLLoader.load(getClass().getClassLoader().getResource(ANSWER_DIALOG_MODAL_FXML));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene answerDialogScene = new Scene(answerDialogWindow);
        Stage newStage = new Stage();
        newStage.initModality(Modality.APPLICATION_MODAL);
        newStage.setTitle(ANSWER_DIALOG_STAGE_TITLE);
        newStage.setResizable(false);
        newStage.setScene(answerDialogScene);
        return newStage;
    }

    public void answerAlgebraicModal(ActionEvent actionEvent) {
        String re = reTextField.getText().replace(",", ".");
        String im = imTextField.getText().replace(",", ".");
        //TODO check re  & im as a number
        boolean answer = MainStageController.getGenerator().checkAnswer(Double.valueOf(re), Double.valueOf(im));
        if (answer) {
            if (rightAnswerStage == null) {
                resultController.setIsAnswerRight(true);
                rightAnswerStage = createAnswerStage();
            }
            answerStage.hide();
            resultController.setAnswerDialogStage(rightAnswerStage);
            rightAnswerStage.show();
        } else {
            if (wrongAnswerStage == null) {
                resultController.setIsAnswerRight(false);
                wrongAnswerStage = createAnswerStage();
            }
            resultController.setAnswerDialogStage(wrongAnswerStage);
            wrongAnswerStage.show();
        }
        clearFields();
    }

    public void cancelAlgebraicModal(ActionEvent actionEvent) {
        answerStage.close();
        clearFields();
    }
}
