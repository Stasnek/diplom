package diplom.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by User on 10.05.2016.
 */
public class Main extends Application {
    private final static String MAIN_STAGE_TITLE = "Complex Generator";

    private final static String MAIN_FXML = "main.fxml";
    public final static String ANSWER_DIALOG_MODAL_FXML = "answerDialogModal.fxml";


    private final static float MIN_HEIGHT = 500;
    private final static float MIN_WIDTH = 600;


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource(MAIN_FXML));
        Scene scene = new Scene(root);

        primaryStage.setTitle(MAIN_STAGE_TITLE);
        primaryStage.setScene(scene);
        primaryStage.show();


    }

    @Override
    public void stop() {
        System.out.println("Stage is closing");
        try {
        } catch (Exception e) {
        }
    }
}
