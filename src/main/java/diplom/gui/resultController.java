package diplom.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by �� on 5/14/2016.
 */
public class resultController implements Initializable {
    private final static String rightAnswerUrl = "img\\rightAnswer.jpg";
    private final static String wrongAnswerUrl = "img\\wrongAnswer.jpg";
    private final static String wrongAnswerMsg = "неправильно!";
    private final static String rightAnswerMsg = "правильно!";

    private static Stage answerDialogStage;
    private static boolean isAnswerRight;
    @FXML
    public ImageView answerImageView;
    @FXML
    public Label messageLabel;

    public static Stage getAnswerDialogStage() {
        return answerDialogStage;
    }

    public static void setAnswerDialogStage(Stage answerDialogStage) {
        resultController.answerDialogStage = answerDialogStage;//answerImageView.setImage(null);
    }

    public static void setIsAnswerRight(boolean value) {
        isAnswerRight = value;
    }

    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("AAAAAAAAAAAAA!1111");
        if (isAnswerRight) {
            answerImageView.setImage(new Image(rightAnswerUrl));
            messageLabel.setText(new String(rightAnswerMsg));
        } else {
            answerImageView.setImage(new Image(wrongAnswerUrl));
            messageLabel.setText(new String(wrongAnswerMsg));
        }

    }

    public void closerAnswerResultStage(ActionEvent actionEvent) {
        answerDialogStage.close();
    }
}
