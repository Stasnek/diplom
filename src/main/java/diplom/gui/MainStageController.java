package diplom.gui;

import diplom.generator.Generator;
import diplom.generator.operations.Operation;
import diplom.generator.operations.impl.recursive.*;
import diplom.generator.operations.impl.recursive.hyperbolic.Cosh;
import diplom.generator.operations.impl.recursive.hyperbolic.Sinh;
import diplom.generator.operations.impl.recursive.hyperbolic.Tanh;
import diplom.generator.operations.impl.recursive.trigonometrical.Cos;
import diplom.generator.operations.impl.recursive.trigonometrical.Sin;
import diplom.generator.operations.impl.recursive.trigonometrical.Tan;
import diplom.generator.operations.impl.recursive.trigonometrical.reversed.ArcCos;
import diplom.generator.operations.impl.recursive.trigonometrical.reversed.ArcSin;
import diplom.generator.operations.impl.recursive.trigonometrical.reversed.ArcTan;
import diplom.generator.operations.impl.simple.Mult;
import diplom.generator.representation.ComplexNumberRepresentation;
import diplom.jLatexMath.JLaTeXMath;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jfxtras.labs.scene.control.BigDecimalField;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by User on 10.05.2016.
 */
public class MainStageController implements Initializable {
    private final static String ANSWER_ALGEBRAICAL_MODAL_FXML = "answerAlgebraicModal.fxml";
    private final static String ANSWER_INDICATIVE_MODAL_FXML = "answerIndicativeModal.fxml";
    private final static String ANSWER_STAGE_TITLE = "Ответ";

    private static Stage answerStageAlgebraic;
    private static Stage answerStageIndicative;

    private static Generator generator;
    private final String ALGEBRAIC_REPRESENTATION = "Алгебраическая";
    private final String INDICATIVE_REPRESENTATION = "Показательная";
    private final String NUMBER_IS_WHOLE = "Целое";
    private final String NUMBER_IS_REAL = "Вещественное";
    @FXML
    public BigDecimalField maxConstantsTextBox;
    @FXML
    public BigDecimalField constantsInExpressionTextBox;
    @FXML
    public BigDecimalField deepTextBox;
    @FXML
    public BigDecimalField maxPowTextBox;
    @FXML
    public ComboBox representationFormComboBox;
    @FXML
    public BigDecimalField maxComplexValueTextBox;
    @FXML
    public ComboBox isWholeComboBox;
    @FXML
    public BigDecimalField roundComplexTetField;
    @FXML
    public CheckBox logCheckBox;
    @FXML
    public CheckBox sqrtCheckBox;
    @FXML
    public CheckBox cosCheckBox;
    @FXML
    public CheckBox sinCheckBox;
    @FXML
    public CheckBox coshCheckBox;
    @FXML
    public CheckBox multCheckBox;
    @FXML
    public CheckBox tanhCheckBox;
    @FXML
    public CheckBox lnCheckBox;
    @FXML
    public CheckBox arctanCheckBox;
    @FXML
    public CheckBox powCheckBox;
    @FXML
    public CheckBox arccosCheckBox;
    @FXML
    public CheckBox divCheckBox;
    @FXML
    public CheckBox sinhCheckBox;
    @FXML
    public CheckBox tanCheckBox;
    @FXML
    public CheckBox arcsinCheckBox;
    @FXML
    public Button resetOperationsButton;
    @FXML
    public ScrollPane operationsScrollPane;
    @FXML
    public Pane operationsPane;
    public BigDecimalField test;
    public RadioButton changeExprRadioBtn;
    public RadioButton changeTopicExprRadioBtn;
    @FXML
    private ImageView imageView;


    public static Generator getGenerator() {
        return generator;
    }

    public static void setGenerator(Generator generator) {
        MainStageController.generator = generator;
    }

    public static Stage getAnswerStageAlgebraic() {
        return answerStageAlgebraic;
    }

    public static void setAnswerStageAlgebraic(Stage answerStageAlgebraic) {
        MainStageController.answerStageAlgebraic = answerStageAlgebraic;
    }

    public static Stage getAnswerStageIndicative() {
        return answerStageIndicative;
    }

    public static void setAnswerStageIndicative(Stage answerStageIndicative) {
        MainStageController.answerStageIndicative = answerStageIndicative;
    }

    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        maxConstantsTextBox.setText(String.valueOf(Generator.CONSTANT_QUANTITY));
        constantsInExpressionTextBox.setText(String.valueOf(Generator.MAX_COMPLEX_NUMBERS_IN_EXPRESSION));
        deepTextBox.setText(String.valueOf(Generator.MAX_DEEP));
        maxPowTextBox.setText(String.valueOf(Generator.MAX_POW_VALUE));
        maxComplexValueTextBox.setText(String.valueOf(Generator.MAX_COMPLEX_VALUE));
        roundComplexTetField.setText(String.valueOf(Generator.ROUND_VALUE));
        isWholeComboBox.getItems().addAll(NUMBER_IS_WHOLE, NUMBER_IS_REAL);
        representationFormComboBox.getItems().addAll(ALGEBRAIC_REPRESENTATION, INDICATIVE_REPRESENTATION);
        changeExprRadioBtn.setSelected(false);
        changeTopicExprRadioBtn.setSelected(true);

        maxConstantsTextBox.setMinValue(BigDecimal.valueOf(2));
        maxConstantsTextBox.setMaxValue(BigDecimal.valueOf(9));
        constantsInExpressionTextBox.setMinValue(BigDecimal.valueOf(2));
        constantsInExpressionTextBox.setMaxValue(BigDecimal.valueOf(999));
        deepTextBox.setMinValue(BigDecimal.valueOf(0));
        deepTextBox.setMaxValue(BigDecimal.valueOf(999));
        maxPowTextBox.setMinValue(BigDecimal.valueOf(2));
        maxPowTextBox.setMaxValue(BigDecimal.valueOf(999));
        maxComplexValueTextBox.setMinValue(BigDecimal.valueOf(0));
        maxComplexValueTextBox.setMaxValue(BigDecimal.valueOf(Integer.MAX_VALUE));
        roundComplexTetField.setMinValue(BigDecimal.valueOf(1));
        roundComplexTetField.setMaxValue(BigDecimal.valueOf(31));
        imageView.setImage(getImage());
    }

    private void prepareGenerator() {
        List<Operation> operations = new ArrayList<Operation>();
        if (multCheckBox.isSelected()) operations.add(new Mult());
        if (divCheckBox.isSelected()) operations.add(new Div());
        if (sinCheckBox.isSelected()) operations.add(new Sin());
        if (cosCheckBox.isSelected()) operations.add(new Cos());
        if (tanCheckBox.isSelected()) operations.add(new Tan());
        if (arccosCheckBox.isSelected()) operations.add(new ArcCos());
        if (arcsinCheckBox.isSelected()) operations.add(new ArcSin());
        if (arctanCheckBox.isSelected()) operations.add(new ArcTan());
        if (sinhCheckBox.isSelected()) operations.add(new Sinh());
        if (coshCheckBox.isSelected()) operations.add(new Cosh());
        if (tanhCheckBox.isSelected()) operations.add(new Tanh());
        if (lnCheckBox.isSelected()) operations.add(new Ln());
        if (logCheckBox.isSelected()) operations.add(new Log());
        if (sqrtCheckBox.isSelected()) operations.add(new Sqrt());
        if (powCheckBox.isSelected()) operations.add(new Pow());

        if (generator == null) {
            setGenerator(new Generator(operations));
        } else {
            generator.setFunctions(operations);
        }

        getGenerator().setConstantQuantity(Integer.parseInt(maxConstantsTextBox.getText()));
        getGenerator().setMaxConstantsInExpression(Integer.parseInt(constantsInExpressionTextBox.getText()));
        getGenerator().setMaxDeep(Integer.parseInt(deepTextBox.getText()));
        getGenerator().setMaxComplexValue(Integer.parseInt(maxComplexValueTextBox.getText()));
        getGenerator().setMaxPowValue(Integer.parseInt(maxPowTextBox.getText()));
        getGenerator().setRoundValue(Integer.parseInt(roundComplexTetField.getText()));

        if (isWholeComboBox.getValue() != null) {
            if (!isWholeComboBox.getValue().toString().equals(NUMBER_IS_WHOLE)) {
                getGenerator().setConstantsWhole(false);
            }
        }

        if (representationFormComboBox.getValue() != null) {
            if (representationFormComboBox.getValue().toString().equals(ALGEBRAIC_REPRESENTATION)) {
                getGenerator().setRepresentation(ComplexNumberRepresentation.ALGEBRAIC);
            } else {
                getGenerator().setRepresentation(ComplexNumberRepresentation.INDICATIVE);
            }
        }
        if (changeExprRadioBtn.isSelected()) {
            getGenerator().setChangeTopicAndExpr(false);
        } else {
            getGenerator().setChangeTopicAndExpr(true);
        }
    }

    private Image getImage() {
        prepareGenerator();
        getGenerator().next();
        BufferedImage bufferedImage = JLaTeXMath.createImage(getGenerator().getTexExpression());
        return SwingFXUtils.toFXImage(bufferedImage, null);
    }

    public void refresh(ActionEvent actionEvent) {
        imageView.setImage(getImage());
    }

    private Stage createAnswerStage() {
        Parent answerWindow = null;
        try {
            if (generator.getRepresentation() == ComplexNumberRepresentation.ALGEBRAIC) {
                answerWindow = FXMLLoader.load(getClass().getClassLoader().getResource(ANSWER_ALGEBRAICAL_MODAL_FXML));
            } else {
                answerWindow = FXMLLoader.load(getClass().getClassLoader().getResource(ANSWER_INDICATIVE_MODAL_FXML));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene answerScene = new Scene(answerWindow);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle(ANSWER_STAGE_TITLE);
        stage.setResizable(false);
        stage.setScene(answerScene);
        stage.hide();
        return stage;
    }

    public void enterTheAnswer(ActionEvent actionEvent) {
        if (generator.getRepresentation() == ComplexNumberRepresentation.ALGEBRAIC) {
            if (answerStageAlgebraic == null) {
                answerStageAlgebraic = createAnswerStage();
            }
            AnswerController.setAnswerStage(answerStageAlgebraic);
            answerStageAlgebraic.show();
        } else {
            if (answerStageIndicative == null) {
                answerStageIndicative = createAnswerStage();
            }
            AnswerController.setAnswerStage(answerStageIndicative);
            answerStageIndicative.show();
        }

    }

    public void resetOperations(ActionEvent actionEvent) {
        setOperationsValue(false);
    }

    public void setAllOperations(ActionEvent actionEvent) {
        setOperationsValue(true);
    }

    private void setOperationsValue(boolean val) {
        for (Node node : operationsPane.getChildren()) {
            CheckBox checkBox = (CheckBox) node;
            checkBox.setSelected(val);
        }
    }

    public void chnageTopicExpr(ActionEvent actionEvent) {
        if (changeTopicExprRadioBtn.isSelected()) {
            changeExprRadioBtn.setSelected(false);
        }
    }

    public void chnageExpr(ActionEvent actionEvent) {
        if (changeExprRadioBtn.isSelected()) {
            changeTopicExprRadioBtn.setSelected(false);
        }

    }
}
