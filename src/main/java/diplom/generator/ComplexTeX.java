package diplom.generator;

import org.nfunk.jep.type.Complex;

/**
 * Created by User on 27.03.2016.
 */
public class ComplexTeX extends Complex {
    private final String IM_IDENT = "i";
    private String identificator;
    private int phi;
    private double r;

    public ComplexTeX(double real, double im, int index) {
        super(real, im);
        setIdentificator(new String("Z_" + index));
    }

    public ComplexTeX(double r, int phi, int index) {
        setIdentificator(new String("Z_" + index));
        this.setPhi(phi);
        this.setR(r);
    }


    public String toStringDetailedWithIdent() {
        if (im() >= 0)
            return new String(identificator + ": " + re() + "+" + im() + IM_IDENT);
        else return new String(identificator + ": " + String.valueOf(re()) + String.valueOf(im()) + IM_IDENT);

    }

    public String toStringIndicative() {
        if (phi > 0) {
            return new String(identificator + ": " + r + "e^{\\frac{\\pi}{" + phi + "}" + IM_IDENT + "}");
        } else {
            return new String(identificator + ": " + r + "e^{-\\frac{\\pi}{" + -phi + "}" + IM_IDENT + "}");
        }
    }


    public String toStringIndicativeJEP() {
        return new String("(" + r + "*e^(" + Math.PI / phi + "*" + IM_IDENT + "))");
    }

    public String toStringJEP() {
        if (im() > 0)
            return new String("(" + re() + "+" + im() + "*" + IM_IDENT + ")");
        else return new String("(" + re() + im() + "*" + IM_IDENT + ")");
    }

    public String toString() {
        return identificator;
    }

    public void setIdentificator(String identificator) {
        this.identificator = identificator;
    }

    public int getPhi() {
        return phi;
    }

    public void setPhi(int phi) {
        this.phi = phi;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }
}