package diplom.generator;

import diplom.explainer.Explainer;
import diplom.generator.operations.Operation;
import diplom.generator.operations.impl.RecursiveOperation;
import diplom.generator.operations.impl.recursive.Div;
import diplom.generator.operations.impl.simple.Minus;
import diplom.generator.operations.impl.simple.Mult;
import diplom.generator.operations.impl.simple.Plus;
import diplom.generator.representation.ComplexNumberRepresentation;
import diplom.generator.util.MathUtil;
import org.nfunk.jep.JEP;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by User on 27.03.2016.
 */
public class Generator {
    //
    public final static int MAX_POW_VALUE = 3;
    public final static int ROUND_VALUE = 2;
    public final static int MAX_COMPLEX_VALUE = 10;
    public final static int MAX_COMPLEX_NUMBERS_IN_EXPRESSION = 10;
    public final static int MAX_DEEP = 2;
    public final static int CONSTANT_QUANTITY = 3;
    public final static boolean IS_CONSTANTS_WHOLE = true;
    public final static ComplexNumberRepresentation REPRESENTATION_FORM = ComplexNumberRepresentation.ALGEBRAIC;


    private static ComplexNumberRepresentation representation = REPRESENTATION_FORM;
    private Random random = new Random();
    private int constantsInExpressionCount = 0;
    private int varCount = 0;
    private int failedExpressionsCounts = 0;
    private JEP parser = new JEP();
    private List<ComplexTeX> constants = new ArrayList<ComplexTeX>();
    private StringBuilder texExpression = new StringBuilder();
    private StringBuilder texTopic = new StringBuilder();
    private StringBuilder jepExpression = new StringBuilder();
    private List<Operation> functions = new ArrayList<Operation>();
    private List<Operation> connectingFunctions = new ArrayList<Operation>();
    private List<Integer> phiValues = Arrays.asList(2, 3, 4, 6);

    private int maxPowValue = MAX_POW_VALUE;
    private int roundValue = ROUND_VALUE;
    private int maxComplexValue = MAX_COMPLEX_VALUE;
    private int constantQuantity = CONSTANT_QUANTITY;
    private int maxConstantsInExpression = MAX_COMPLEX_NUMBERS_IN_EXPRESSION;
    private int maxDeep = MAX_DEEP;
    private boolean isConstantsWhole = IS_CONSTANTS_WHOLE;
    private boolean changeTopicAndExpr = true;

    public Generator(List<Operation> operations) {
        getParser().addComplex();
        getParser().addStandardFunctions();
        getParser().addStandardConstants();

        getFunctions().add(new Plus());
        getFunctions().add(new Minus());

        getFunctions().addAll(operations);
        getConnectingFunctions().add(new Plus());
        getConnectingFunctions().add(new Minus());
        getConnectingFunctions().add(new Mult());

    }

    public static ComplexNumberRepresentation getRepresentation() {
        return representation;
    }

    public static void setRepresentation(ComplexNumberRepresentation represent) {
        representation = represent;
    }

    public boolean isInRange(double rightAnswer, double userAnswer) {
        String value = String.valueOf(userAnswer);
        int index = value.length() - value.lastIndexOf(".") - 1;

        if (MathUtil.truncate(rightAnswer, index) == userAnswer || userAnswer == MathUtil.round(rightAnswer, index)) {
            return true;
        }
        return false;
    }

    public boolean checkAnswer(double firstArg, double secondArg) {

        boolean firstArgInRange = false;
        boolean secondArgInRange = false;

        double re = getParser().getComplexValue().re();
        double im = getParser().getComplexValue().im();

        if (getRepresentation() == ComplexNumberRepresentation.ALGEBRAIC) {
            firstArgInRange = isInRange(re, firstArg);
            secondArgInRange = isInRange(im, secondArg);
        }

        if (getRepresentation() == ComplexNumberRepresentation.INDICATIVE) {
            double phi = Math.atan2(im, re);
            double r = Math.sqrt(Math.pow(re, 2) + Math.pow(im, 2));
            firstArgInRange = isInRange(r, firstArg);
            secondArgInRange = isInRange(phi, secondArg);
        }
        if (firstArgInRange & secondArgInRange) {
            return true;
        }
        return false;
    }

    public int generatePow() {
        int pow = 1;
        if (getMaxPowValue() > 1) {
            do {
                pow = getRandom().nextInt(getMaxPowValue());
            } while (pow == 0 || pow == 1);
        }
        return pow;
    }

    private ComplexTeX generateComplexNumber() {
        Double realPart = 0d;
        Double imaginaryPart = 0d;
        if (getMaxComplexValue() > 0) {
            realPart = (double) getRandom().nextInt(getMaxComplexValue());
            imaginaryPart = (double) getRandom().nextInt(getMaxComplexValue());
        }
        if (!isConstantsWhole()) {
            realPart += getRandom().nextDouble();
            imaginaryPart += getRandom().nextDouble();
            realPart = MathUtil.round(realPart, getRoundValue());
            imaginaryPart = MathUtil.round(imaginaryPart, getRoundValue());
        }
        if (!getRandom().nextBoolean() & realPart != 0) {
            realPart = -realPart;
        }

        if (!getRandom().nextBoolean() & imaginaryPart != 0) {
            imaginaryPart = -imaginaryPart;
        }

        return new ComplexTeX(realPart, imaginaryPart, getVarCount());
    }

    private ComplexTeX generateIndicativeComplexNumber() {
        int phi = randomizePhi();

        double r;
        do {
            r = getRandom().nextInt(getMaxComplexValue());
        } while (r == 0);

        if (!isConstantsWhole()) {
            r += getRandom().nextDouble();
            r = MathUtil.round(r, getRoundValue());
        }
        if (!getRandom().nextBoolean()) {
            r = -r;
        }
        if (!getRandom().nextBoolean()) {
            phi = -phi;
        }
        return new ComplexTeX(r, phi, getVarCount());
    }

    private void generateConstants() {
        ComplexTeX complexNumber = null;
        for (int i = 0; i < getConstantQuantity(); i++) {
            if (getRepresentation() == ComplexNumberRepresentation.ALGEBRAIC) {
                complexNumber = generateComplexNumber();
            } else {
                complexNumber = generateIndicativeComplexNumber();
            }
            getConstants().add(complexNumber);
            getParser().addVariableAsObject(complexNumber.toString(), complexNumber);
        }

    }

    private void generateTopic() {
        if (getRepresentation() == ComplexNumberRepresentation.ALGEBRAIC) {
            for (int i = 0; i < getConstantQuantity(); i++) {
                getTexTopic().append(getConstants().get(i).toStringDetailedWithIdent()).append("\\\\");
            }
        } else {
            for (int i = 0; i < getConstantQuantity(); i++) {
                getTexTopic().append(getConstants().get(i).toStringIndicative()).append("\\\\");
            }
        }
    }

    private Boolean hasNext(ExpressionNode node) {
        int value = getRandom().nextInt(getMaxDeep());
        int level = node.getLevel();

        if (value > level) {
            return true;
        }
        return false;
    }

    private Operation randomizeOperation() {
        return getFunctions().get(getRandom().nextInt(getFunctions().size()));
    }

    private Integer randomizePhi() {
        return getPhiValues().get(getRandom().nextInt(getPhiValues().size()));
    }

    private Operation randomizeConnectingOperation() {
        return getConnectingFunctions().get(getRandom().nextInt(getConnectingFunctions().size()));
    }

    private ComplexTeX randomizeIdentificator(ExpressionNode node) {
        int constantIndex = getRandom().nextInt(getConstantQuantity());

        System.out.println("level : " + node.getLevel() + " : " + getConstants().get(constantIndex).toString() + " i: " + getConstantsInExpressionCount());
        return getConstants().get(constantIndex);
    }

    private void updateNode(ExpressionNode node, ExpressionNode subNode) {
        node.setDeep(subNode.getDeep());
    }

    private void addConcatenationOperation(ExpressionNode node) {
        Operation func = randomizeConnectingOperation();
        node.addFunction(func);
        func.make(node);
    }

    private void serveOperation(ExpressionNode node) {
        node.levelInc();
        node.deepInc();
    }

    private void makeOperation(ExpressionNode node) {
        Operation func = randomizeOperation();
        if (RecursiveOperation.class.isInstance(func)) {
            if (node.getDeep() < getMaxDeep() - 1) {
                serveOperation(node);
                if (Div.class.isInstance(func)) {
                    node.addFunction(func);
                    ExpressionNode subNode = generateExpression(new ExpressionNode(node));
                    func.make(node, subNode);
                    updateNode(node, subNode);
                } else if (getRandom().nextBoolean()) {
                    addConcatenationOperation(node);
                    node.addFunction(func);
                    ExpressionNode subNode = generateExpression(new ExpressionNode(node));
                    func.make(node, subNode);
                    updateNode(node, subNode);
                } else {
                    node.addFunction(func);
                    func.make(node);
                }
                if (getConstantsInExpressionCount() < getMaxConstantsInExpression()) {
                    node.levelDec();
                    makeOperation(node);
                }
            } else {
                makeOperation(node);
            }
        } else {
            node.addFunction(func);
            func.make(node);
        }
    }

    private ExpressionNode generateExpression(ExpressionNode node) {
        if (getConstantsInExpressionCount() < getMaxConstantsInExpression() & node.getLevel() < getMaxDeep()) {
            setConstantsInExpressionCount(getConstantsInExpressionCount() + 1);
            node.appendConstant(randomizeIdentificator(node));
            if (!hasNext(node)) {
                if (node.getLevel() == 0 & getConstantsInExpressionCount() != getMaxConstantsInExpression()) {
                    makeOperation(node);
                }
                return node;
            } else {
                if (getConstantsInExpressionCount() < getMaxConstantsInExpression()) {
                    makeOperation(node);
                    generateExpression(node);
                }
            }
        }
        return node;
    }

    public void next() {
        failedExpressionsCounts = 0;
        do {
            clearData();
            if (changeTopicAndExpr) {
                generateConstants();
                generateTopic();
            }
            List<ExpressionNode> expressionNodes = new ArrayList<ExpressionNode>();
            int nodesCount = 0;
            while (getConstantsInExpressionCount() < getMaxConstantsInExpression()) {
                expressionNodes.add(generateExpression(new ExpressionNode(this)));
                nodesCount++;
                System.out.println("\nnode : " + nodesCount);
            }
            System.out.println(expressionNodes);
            for (ExpressionNode node : expressionNodes) {
                getJepExpression().append(node.getJepExpression());
                texExpression.append(node.getTexExpression());
                node.printInfo();
            }
            getParser().parseExpression(new String(getJepExpression()));
            getParser().getErrorInfo();
            System.out.println(getJepExpression());
        } while (!checkResult());
        Explainer.test(this);
        System.out.println("failed " + failedExpressionsCounts);
    }

    private void clearData() {
        if (changeTopicAndExpr) {
            getConstants().clear();
            setTexTopic(new StringBuilder());
        }
        setJepExpression(new StringBuilder());
        setTexExpression(new StringBuilder());
        setConstantsInExpressionCount(0);
        setVarCount(0);
    }

    private boolean checkResult() {
        Double re = getParser().getComplexValue().re();
        Double im = getParser().getComplexValue().im();
        // System.out.println("\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"+re+" "+im);

        if (re.equals(Double.NaN) || im.equals(Double.NaN)
                || re.equals(Double.NEGATIVE_INFINITY) || im.equals(Double.NEGATIVE_INFINITY)
                || re.equals(Double.POSITIVE_INFINITY) || im.equals(Double.POSITIVE_INFINITY)) {
            failedExpressionsCounts++;
            return false;
        }
        return true;
    }

    public String getTexExpression() {
        if (getRepresentation() == ComplexNumberRepresentation.ALGEBRAIC) {
            return "$" + getTexTopic() + "\\\\" + texExpression + "\\\\" + getParser().getComplexValue() + "$";
        } else {
            double re = getParser().getComplexValue().re();
            double im = getParser().getComplexValue().im();

            double phi2 = Math.atan2(im, re);
//            double phi = Math.atan(im / re);

            double r = Math.sqrt(Math.pow(re, 2) + Math.pow(im, 2));
            return "$" + getTexTopic() + "\\\\" + texExpression + "\\\\" + "(" + r + "," + phi2 + ")$";
//            return "$" + texTopic + "\\\\" + texExpression + "\\\\" + "(r " + r + ",phi2    " + phi2 + ",phi   "+phi+")" +"\\\\ang2 "+Math.toDegrees(phi2)+"\\\\ang  "+Math.toDegrees(phi)+
//                   "\\\\ re "+re+
//                    "\\\\ im "+im+
//                    "$";
        }
    }

    public int getMaxPowValue() {
        return maxPowValue;
    }

    public void setMaxPowValue(int maxPowValue) {
        this.maxPowValue = ++maxPowValue;
    }

    public int getRoundValue() {
        return roundValue;
    }

    public void setRoundValue(int roundValue) {
        this.roundValue = roundValue;
    }

    public int getMaxComplexValue() {
        return maxComplexValue;
    }

    public void setMaxComplexValue(int maxComplexValue) {
        this.maxComplexValue = ++maxComplexValue;
    }

    public int getConstantQuantity() {
        return constantQuantity;
    }

    public void setConstantQuantity(int constantQuantity) {
        this.constantQuantity = constantQuantity;
    }

    public int getMaxConstantsInExpression() {
        return maxConstantsInExpression;
    }

    public void setMaxConstantsInExpression(int maxConstantsInExpression) {
        this.maxConstantsInExpression = maxConstantsInExpression;
    }

    public int getMaxDeep() {
        return maxDeep;
    }

    public void setMaxDeep(int maxDeep) {
        this.maxDeep = ++maxDeep;
    }

    public boolean isConstantsWhole() {
        return isConstantsWhole;
    }

    public void setConstantsWhole(boolean constantsWhole) {
        setIsConstantsWhole(constantsWhole);
    }

    public JEP getParser() {
        return parser;
    }

    public void setParser(JEP parser) {
        this.parser = parser;
    }

    public Random getRandom() {
        return random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public int getConstantsInExpressionCount() {
        return constantsInExpressionCount;
    }

    public void setConstantsInExpressionCount(int constantsInExpressionCount) {
        this.constantsInExpressionCount = constantsInExpressionCount;
    }

    public int getVarCount() {
        return ++varCount;
    }

    public void setVarCount(int varCount) {
        this.varCount = varCount;
    }

    public List<ComplexTeX> getConstants() {
        return constants;
    }

    public void setConstants(List<ComplexTeX> constants) {
        this.constants = constants;
    }

    public void setTexExpression(StringBuilder texExpression) {
        this.texExpression = texExpression;
    }

    public StringBuilder getTexTopic() {
        return texTopic;
    }

    public void setTexTopic(StringBuilder texTopic) {
        this.texTopic = texTopic;
    }

    public StringBuilder getJepExpression() {
        return jepExpression;
    }

    public void setJepExpression(StringBuilder jepExpression) {
        this.jepExpression = jepExpression;
    }

    public List<Operation> getFunctions() {
        return functions;
    }

    public void setFunctions(List<Operation> functions) {
        this.functions = functions;
        this.functions.add(new Plus());
        this.functions.add(new Minus());
    }

    public List<Operation> getConnectingFunctions() {
        return connectingFunctions;
    }

    public void setConnectingFunctions(List<Operation> connectingFunctions) {
        this.connectingFunctions = connectingFunctions;
    }

    public List<Integer> getPhiValues() {
        return phiValues;
    }

    public void setPhiValues(List<Integer> phiValues) {
        this.phiValues = phiValues;
    }

    public void setIsConstantsWhole(boolean isConstantsWhole) {
        this.isConstantsWhole = isConstantsWhole;
    }

    public boolean isChangeTopicAndExpr() {
        return changeTopicAndExpr;
    }

    public void setChangeTopicAndExpr(boolean changeTopicAndExpr) {
        this.changeTopicAndExpr = changeTopicAndExpr;
    }
}
