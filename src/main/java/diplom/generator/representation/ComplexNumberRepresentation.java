package diplom.generator.representation;

/**
 * Created by User on 24.05.2016.
 */
public enum ComplexNumberRepresentation {
    ALGEBRAIC, INDICATIVE
}
