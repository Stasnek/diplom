package diplom.generator.operations;

import diplom.generator.ExpressionNode;

/**
 * Created by User on 02.05.2016.
 */
public interface Operation {
    void make(ExpressionNode node);

    void make(ExpressionNode node, ExpressionNode subNode);

    String toString();
}