package diplom.generator.operations.impl.simple;

import diplom.generator.ExpressionNode;
import diplom.generator.operations.impl.SimpleOperation;

/**
 * Created by User on 02.05.2016.
 */
public class Minus extends SimpleOperation {
    public void make(ExpressionNode node) {
        node.appendToTeX("-");
        node.appendToJEP("-");
    }

    public void make(ExpressionNode node, ExpressionNode subNode) {

    }
}
