package diplom.generator.operations.impl;

import diplom.generator.operations.Operation;

/**
 * Created by User on 02.05.2016.
 */
public abstract class SimpleOperation implements Operation {
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
