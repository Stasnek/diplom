package diplom.generator.operations.impl.recursive;

import diplom.generator.ExpressionNode;
import diplom.generator.operations.impl.RecursiveOperation;

/**
 * Created by User on 04.05.2016.
 */
public class Ln extends RecursiveOperation {
    public void make(ExpressionNode node) {
        node.insertToTeX(0, "\\ln\\left(");
        node.appendToTeX("\\right)");

        node.insertToJEP(0, "ln(");
        node.appendToJEP(")");
    }

    public void make(ExpressionNode node, ExpressionNode subNode) {
        node.appendToTeX("\\ln\\left(");
        node.appendToTeX(String.valueOf(subNode.getTexExpression()));
        node.appendToTeX("\\right)");

        node.appendToJEP("ln");
        node.appendToJEP("(");
        node.appendToJEP(new String(subNode.getJepExpression()));
        node.appendToJEP(")");
    }
}
