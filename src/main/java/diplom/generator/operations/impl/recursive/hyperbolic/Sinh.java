package diplom.generator.operations.impl.recursive.hyperbolic;

import diplom.generator.ExpressionNode;
import diplom.generator.operations.impl.RecursiveOperation;

/**
 * Created by User on 04.05.2016.
 */
public class Sinh extends RecursiveOperation {
    public void make(ExpressionNode node) {
        node.insertToTeX(0, "\\sinh\\left(");
        node.appendToTeX("\\right)");

        node.insertToJEP(0, "sinh(");
        node.appendToJEP(")");
    }

    public void make(ExpressionNode node, ExpressionNode subNode) {
        node.appendToTeX("\\sinh\\left(");
        node.appendToTeX(String.valueOf(subNode.getTexExpression()));
        node.appendToTeX("\\right)");

        node.appendToJEP("sinh");
        node.appendToJEP("(");
        node.appendToJEP(new String(subNode.getJepExpression()));
        node.appendToJEP(")");
    }
}
