package diplom.generator.operations.impl.recursive;

import diplom.generator.ExpressionNode;
import diplom.generator.operations.impl.RecursiveOperation;

/**
 * Created by User on 04.05.2016.
 */
public class Pow extends RecursiveOperation {

    public void make(ExpressionNode node) {
        int pow = node.getGenerator().generatePow();
        if (pow > 1) {
            //  if (node.getConstants().size() > 1) {
            node.insertToTeX(0, "\\left(");
            node.appendToTeX("\\right)^{" + pow + "}");
//            } else {
//                node.appendToTeX("^{" + pow + "}");
//            }

            node.insertToJEP(0, "(");
            node.appendToJEP(")^(" + pow + ")");
        }
    }

    public void make(ExpressionNode node, ExpressionNode subNode) {
        int pow = node.getGenerator().generatePow();
        if (pow > 1) {
            // if (subNode.getConstants().size() > 1) {
            node.appendToTeX("\\left(");
            node.appendToTeX(String.valueOf(subNode.getTexExpression()));
            node.appendToTeX("\\right)^{" + pow + "}");
//            } else {
//                node.appendToTeX(String.valueOf(subNode.getTexExpression()));
//                node.appendToTeX("^{" + pow + "}");
//            }
            node.appendToJEP("(");
            node.appendToJEP(String.valueOf(subNode.getJepExpression()));
            node.appendToJEP(")^(" + pow + ")");
        }
    }
}
