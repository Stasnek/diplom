package diplom.generator.operations.impl.recursive.trigonometrical.reversed;

import diplom.generator.ExpressionNode;
import diplom.generator.operations.impl.RecursiveOperation;

/**
 * Created by User on 04.05.2016.
 */
public class ArcSin extends RecursiveOperation {
    public void make(ExpressionNode node) {
        node.insertToTeX(0, "\\arcsin\\left(");
        node.appendToTeX("\\right)");

        node.insertToJEP(0, "asin(");
        node.appendToJEP(")");
    }

    public void make(ExpressionNode node, ExpressionNode subNode) {
        node.appendToTeX("\\arcsin\\left(");
        node.appendToTeX(String.valueOf(subNode.getTexExpression()));
        node.appendToTeX("\\right)");

        node.appendToJEP("asin");
        node.appendToJEP("(");
        node.appendToJEP(new String(subNode.getJepExpression()));
        node.appendToJEP(")");
    }
}
