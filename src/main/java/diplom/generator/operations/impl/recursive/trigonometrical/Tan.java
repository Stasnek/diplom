package diplom.generator.operations.impl.recursive.trigonometrical;

import diplom.generator.ExpressionNode;
import diplom.generator.operations.impl.RecursiveOperation;

/**
 * Created by User on 03.05.2016.
 */
public class Tan extends RecursiveOperation {
    public void make(ExpressionNode node) {
        node.insertToTeX(0, "\\tan\\left(");
        node.appendToTeX("\\right)");

        node.insertToJEP(0, "tan(");
        node.appendToJEP(")");
    }

    public void make(ExpressionNode node, ExpressionNode subNode) {
        node.appendToTeX("\\tan\\left(");
        node.appendToTeX(String.valueOf(subNode.getTexExpression()));
        node.appendToTeX("\\right)");

        node.appendToJEP("tan");
        node.appendToJEP("(");
        node.appendToJEP(new String(subNode.getJepExpression()));
        node.appendToJEP(")");
    }
}
