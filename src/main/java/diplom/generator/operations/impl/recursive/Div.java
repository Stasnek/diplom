package diplom.generator.operations.impl.recursive;

import diplom.generator.ExpressionNode;
import diplom.generator.operations.impl.RecursiveOperation;

/**
 * Created by User on 03.05.2016.
 */
public class Div extends RecursiveOperation {

    public void make(ExpressionNode node) {

    }

    public void make(ExpressionNode node, ExpressionNode subNode) {
        node.insertToTeX(0, "\\left(\\frac{").appendToTeX("}{");
        node.appendToTeX(String.valueOf(subNode.getTexExpression()));
        node.appendToTeX("}\\right)");

        node.insertToJEP(0, "(");
        node.appendToJEP(")");
        node.appendToJEP("/");

        node.appendToJEP("(");
        node.appendToJEP(new String(subNode.getJepExpression()));
        node.appendToJEP(")");
    }


}
