package diplom.generator.operations.impl.recursive;

import diplom.generator.ExpressionNode;
import diplom.generator.operations.impl.RecursiveOperation;

/**
 * Created by User on 04.05.2016.
 */
public class Log extends RecursiveOperation {
    public void make(ExpressionNode node) {
        node.insertToTeX(0, "\\log\\left(");
        node.appendToTeX("\\right)");

        node.insertToJEP(0, "log(");
        node.appendToJEP(")");
    }

    public void make(ExpressionNode node, ExpressionNode subNode) {
        node.appendToTeX("\\log\\left(");
        node.appendToTeX(String.valueOf(subNode.getTexExpression()));
        node.appendToTeX("\\right)");

        node.appendToJEP("log");
        node.appendToJEP("(");
        node.appendToJEP(new String(subNode.getJepExpression()));
        node.appendToJEP(")");
    }
}
