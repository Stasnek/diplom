package diplom.generator.operations.impl.recursive.trigonometrical.reversed;

import diplom.generator.ExpressionNode;
import diplom.generator.operations.impl.RecursiveOperation;

/**
 * Created by User on 04.05.2016.
 */
public class ArcCos extends RecursiveOperation {
    public void make(ExpressionNode node) {
        node.insertToTeX(0, "\\arccos\\left(");
        node.appendToTeX("\\right)");

        node.insertToJEP(0, "acos(");
        node.appendToJEP(")");
    }

    public void make(ExpressionNode node, ExpressionNode subNode) {
        node.appendToTeX("\\arccos\\left(");
        node.appendToTeX(String.valueOf(subNode.getTexExpression()));
        node.appendToTeX("\\right)");

        node.appendToJEP("acos");
        node.appendToJEP("(");
        node.appendToJEP(new String(subNode.getJepExpression()));
        node.appendToJEP(")");
    }
}
