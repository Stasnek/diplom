package diplom.generator.operations.impl.recursive.trigonometrical;

import diplom.generator.ExpressionNode;
import diplom.generator.operations.impl.RecursiveOperation;

/**
 * Created by User on 03.05.2016.
 */
public class Cos extends RecursiveOperation {
    public void make(ExpressionNode node) {
        node.insertToTeX(0, "\\cos\\left(");
        node.appendToTeX("\\right)");

        node.insertToJEP(0, "cos(");
        node.appendToJEP(")");
    }

    public void make(ExpressionNode node, ExpressionNode subNode) {
        node.appendToTeX("\\cos\\left(");
        node.appendToTeX(String.valueOf(subNode.getTexExpression()));
        node.appendToTeX("\\right)");

        node.appendToJEP("cos");
        node.appendToJEP("(");
        node.appendToJEP(new String(subNode.getJepExpression()));
        node.appendToJEP(")");

    }
}
