package diplom.generator.operations.impl.recursive.trigonometrical;

import diplom.generator.ExpressionNode;
import diplom.generator.operations.impl.RecursiveOperation;

/**
 * Created by User on 02.05.2016.
 */


public class Sin extends RecursiveOperation {
    public void make(ExpressionNode node) {
        node.insertToTeX(0, "\\sin\\left(");
        node.appendToTeX("\\right)");

        node.insertToJEP(0, "sin(");
        node.appendToJEP(")");
    }

    public void make(ExpressionNode node, ExpressionNode subNode) {
        node.appendToTeX("\\sin\\left(");
        node.appendToTeX(String.valueOf(subNode.getTexExpression()));
        node.appendToTeX("\\right)");

        node.appendToJEP("sin");
        node.appendToJEP("(");
        node.appendToJEP(new String(subNode.getJepExpression()));
        node.appendToJEP(")");
    }
}
