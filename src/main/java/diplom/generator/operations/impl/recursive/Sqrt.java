package diplom.generator.operations.impl.recursive;

import diplom.generator.ExpressionNode;
import diplom.generator.operations.impl.RecursiveOperation;

/**
 * Created by User on 04.05.2016.
 */
public class Sqrt extends RecursiveOperation {
    public void make(ExpressionNode node) {
        node.insertToTeX(0, "\\sqrt{");
        node.appendToTeX("}");

        node.insertToJEP(0, "sqrt(");
        node.appendToJEP(")");

    }

    public void make(ExpressionNode node, ExpressionNode subNode) {
        node.appendToTeX("\\sqrt{");
        node.appendToTeX(String.valueOf(subNode.getTexExpression()));
        node.appendToTeX("}");

        node.appendToJEP("sqrt");

        node.appendToJEP("(");
        node.appendToJEP(new String(subNode.getJepExpression()));
        node.appendToJEP(")");
    }
}
