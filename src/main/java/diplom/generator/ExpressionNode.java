package diplom.generator;

import diplom.generator.operations.Operation;
import diplom.generator.representation.ComplexNumberRepresentation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 25.04.2016.
 */
public class ExpressionNode {
    private StringBuilder texExpression = new StringBuilder();
    private int level = 0;
    private int deep = 0;
    private StringBuilder jepExpression = new StringBuilder();
    private List<Operation> functions = new ArrayList<Operation>();
    private List<ComplexTeX> constants = new ArrayList<ComplexTeX>();
    private Generator generator;

    private List<String> expressionSeq = new ArrayList<String>();

    public ExpressionNode(ExpressionNode node) {
        level = node.getLevel();
        deep = node.getDeep();
        functions = node.getFunctions();
        //  constants = node.getConstants();
        expressionSeq = node.getExpressionSeq();
        generator = node.getGenerator();
    }

    public ExpressionNode(Generator generator) {
        this.setGenerator(generator);
    }


    public List<String> getExpressionSeq() {
        return expressionSeq;
    }

    public void setExpressionSeq(List<String> expressionSeq) {
        this.expressionSeq = expressionSeq;
    }


    public int getDeep() {
        return deep;
    }

    public void setDeep(int deep) {
        this.deep = deep;
    }

    public void deepInc() {
        deep++;
    }

    public void levelInc() {
        level++;
    }

    public void levelDec() {
        level--;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public List<Operation> getFunctions() {
        return functions;
    }

    public void setFunctions(List<Operation> functions) {
        this.functions = functions;
    }

    public StringBuilder getTexExpression() {
        return texExpression;
    }

    public StringBuilder getJepExpression() {
        return jepExpression;
    }

    public void addFunction(Operation func) {
        functions.add(func);
        this.expressionSeq.add(func.toString());
    }

    public void addConstant(ComplexTeX constant) {
        constants.add(constant);
    }


    public ExpressionNode appendToTeX(String str) {
        texExpression.append(str);
        return this;
    }

    public ExpressionNode appendToJEP(String str) {
        jepExpression.append(str);
        return this;
    }

    public ExpressionNode insertToTeX(int index, String str) {
        texExpression.insert(index, str);
        return this;
    }

    public ExpressionNode insertToJEP(int index, String str) {
        jepExpression.insert(index, str);
        return this;
    }

    public String toString() {
        return String.valueOf(texExpression);
    }

    public void printInfo() {
        System.out.println("\nlevel : " + level);
        System.out.println("deep : " + deep);
        System.out.println("TeXexpr : " + texExpression);
        System.out.println("JEPexpr : " + jepExpression);
        System.out.println("funcs : " + functions);
        System.out.print("constants : ");
        System.out.println(constants);
        System.out.print("sequence : ");
        System.out.println(expressionSeq);
    }

    public void appendConstant(ComplexTeX randomIdentificator) {
        this.appendToTeX(randomIdentificator.toString());
        if (Generator.getRepresentation() == ComplexNumberRepresentation.ALGEBRAIC) {
            this.appendToJEP(randomIdentificator.toString());
        } else {
            this.appendToJEP(randomIdentificator.toStringIndicativeJEP());
        }
        this.addConstant(randomIdentificator);
        this.expressionSeq.add(randomIdentificator.toString());
    }

    public void appendConstantJEP(ComplexTeX randomIdentificator) {
        this.appendToJEP(randomIdentificator.toString());
    }

    public List<ComplexTeX> getConstants() {
        return constants;
    }

    public void setConstants(List<ComplexTeX> constants) {
        this.constants = constants;
    }

    public Generator getGenerator() {
        return generator;
    }

    public void setGenerator(Generator generator) {
        this.generator = generator;
    }
}
